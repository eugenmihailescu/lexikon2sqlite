import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
/**
 * ------------------------------------------------------------
 *  This file is part of lexikon2sqlite.
 *
 *  NadaParser is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NadaParser is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NadaParser.  If not, see <http://www.gnu.org/licenses/>.
 * ------------------------------------------------------------
 *
 * SVN revision information:
 * @version $Revision: 5f4a4b0 $:
 * @author  $Author: Eugen Mihailescu <eugenmihailescux@gmail.com> $:
 * @date    $Date:   Thu May 3 20:00:44 2012 +0200 $:
 */

class FolketLexiconSaxParser extends DefaultHandler {
	static Compound compound;
	static Definition definition;
	static Derivation derivation;
	static Example example;
	static Explanation explanation;
	static Idiom idiom;
	static Paradigm paradigm;
	static String parentTag;
	static Related related;
	static See see;
	static Word word;
	Dictionary dictionary;

	@Override
	public void startDocument() throws SAXException {
		dictionary = new Dictionary();
		parentTag = "";
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attrs) throws SAXException {

		if (qName.compareToIgnoreCase("dictionary") == 0) {
			dictionary.comment = attrs.getValue("comment");
			dictionary.created = attrs.getValue("created");
			dictionary.last_changed = attrs.getValue("last_changed");
			dictionary.name = attrs.getValue("name");
			dictionary.source_language = attrs.getValue("source_language");
			dictionary.target_language = attrs.getValue("target_language");
			dictionary.version = attrs.getValue("version");
			dictionary.license = attrs.getValue("license");
			dictionary.licenseComment = attrs.getValue("licenseComment");
			dictionary.originURL = attrs.getValue("originURL");
		} else if (qName.compareToIgnoreCase("word") == 0) {
			word = new Word();
			word.comment = attrs.getValue("comment");
			word.class_ = attrs.getValue("class");
			word.lang = attrs.getValue("lang");
			word.value = attrs.getValue("value");
			dictionary.word.add(word);
		} else if (qName.compareToIgnoreCase("translation") == 0) {
			Translation translation = new Translation();
			translation.value = attrs.getValue("value");
			translation.comment = attrs.getValue("comment");
			if (parentTag.compareTo("see") == 0)
				see.translation.add(translation);
			else if (parentTag.compareTo("example") == 0)
				example.translation.add(translation);
			else if (parentTag.compareTo("definition") == 0)
				definition.translation.add(translation);
			else if (parentTag.compareTo("idiom") == 0)
				idiom.translation.add(translation);
			else if (parentTag.compareTo("related") == 0)
				related.translation.add(translation);
			else if (parentTag.compareTo("explanation") == 0)
				explanation.translation.add(translation);
			else if (parentTag.compareTo("paradigm") == 0)
				paradigm.translation.add(translation);
			else if (parentTag.compareTo("compound") == 0)
				compound.translation.add(translation);
			else if (parentTag.compareTo("derivation") == 0)
				derivation.translation.add(translation);
			else
				word.translation.add(translation);
		} else if (qName.compareToIgnoreCase("phonetic") == 0) {
			Phonetic phonetic = new Phonetic();
			phonetic.soundFile = attrs.getValue("soundFile");
			phonetic.value = attrs.getValue("value");
			word.phonetic.add(phonetic);
		} else if (qName.compareToIgnoreCase("see") == 0) {
			see = new See();
			see.type = attrs.getValue("type");
			see.value = attrs.getValue("value");
			word.see.add(see);
		} else if (qName.compareToIgnoreCase("example") == 0) {
			example = new Example();
			example.value = attrs.getValue("value");
			word.example.add(example);
		} else if (qName.compareToIgnoreCase("definition") == 0) {
			definition = new Definition();
			definition.value = attrs.getValue("value");
			word.definition.add(definition);
		} else if (qName.compareToIgnoreCase("idiom") == 0) {
			idiom = new Idiom();
			idiom.value = attrs.getValue("value");
			word.idiom.add(idiom);
		} else if (qName.compareToIgnoreCase("related") == 0) {
			related = new Related();
			related.comment = attrs.getValue("comment");
			related.type = attrs.getValue("type");
			related.value = attrs.getValue("value");
			word.related.add(related);
		} else if (qName.compareToIgnoreCase("explanation") == 0) {
			explanation = new Explanation();
			explanation.value = attrs.getValue("value");
			word.explanation.add(explanation);
		} else if (qName.compareToIgnoreCase("paradigm") == 0) {
			paradigm = new Paradigm();
			paradigm.comment = attrs.getValue("comment");
			word.paradigm.add(paradigm);
		} else if (qName.compareToIgnoreCase("compound") == 0) {
			compound = new Compound();
			compound.comment = attrs.getValue("comment");
			compound.value = attrs.getValue("value");
			word.compound.add(compound);
		} else if (qName.compareToIgnoreCase("url") == 0) {
			URL url = new URL();
			url.type = attrs.getValue("type");
			url.value = attrs.getValue("value");
			word.url.add(url);
		} else if (qName.compareToIgnoreCase("synonym") == 0) {
			Synonym synonym = new Synonym();
			synonym.level = attrs.getValue("level");
			synonym.value = attrs.getValue("value");
			word.synonym.add(synonym);
		} else if (qName.compareToIgnoreCase("derivation") == 0) {
			derivation = new Derivation();
			derivation.value = attrs.getValue("value");
			word.derivation.add(derivation);
		} else if (qName.compareToIgnoreCase("grammar") == 0) {
			Grammar grammar = new Grammar();
			grammar.value = attrs.getValue("value");
			grammar.comment = attrs.getValue("comment");
			word.grammar.add(grammar);
		} else if (qName.compareToIgnoreCase("variant") == 0) {
			Variant variant = new Variant();
			variant.alt = attrs.getValue("alt");
			variant.value = attrs.getValue("value");
			variant.comment = attrs.getValue("comment");
			word.variant.add(variant);
		} else if (qName.compareToIgnoreCase("use") == 0) {
			Use use = new Use();
			use.value = attrs.getValue("value");
			word.use.add(use);
		} else if (qName.compareToIgnoreCase("inflection") == 0) {
			Inflection inflection = new Inflection();
			inflection.comment = attrs.getValue("comment");
			inflection.value = attrs.getValue("value");
			paradigm.inflection.add(inflection);
		}
		parentTag = qName;
	}
}
