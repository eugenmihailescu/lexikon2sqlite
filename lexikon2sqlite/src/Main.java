import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
/**
 * ------------------------------------------------------------
 *  This file is part of lexikon2sqlite.
 *
 *  NadaParser is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  NadaParser is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with NadaParser.  If not, see <http://www.gnu.org/licenses/>.
 * ------------------------------------------------------------
 *
 * SVN revision information:
 * @version $Revision: 5f4a4b0 $:
 * @author  $Author: Eugen Mihailescu <eugenmihailescux@gmail.com> $:
 * @date    $Date:   Thu May 3 20:00:44 2012 +0200 $:
 */

public class Main {

	static long start;
	private static Boolean verbose;

	public static void DONE() {
		if (verbose) {
			System.out.println("DONE");
			long elapsedTime = System.nanoTime() - start;
			System.out.println("Time spent: " + Long.toString(elapsedTime / 1000000000) + "sec");

		}
	}

	public static void ERROR(Exception e) {
		if (verbose) {
			System.out.println("ERROR");
			e.printStackTrace();
		}
	}

	private static Boolean fileExists(String fileName) {
		File file = new File(fileName);
		return file.exists();
	}

	public static int getFileCount(String filename) {
		BufferedReader reader;
		int lines = 0;
		try {
			reader = new BufferedReader(new FileReader(filename));
			String line = reader.readLine();

			while (line != null) {
				if (!line.startsWith("#") && line.endsWith(";"))
					lines++;
				line = reader.readLine();
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lines;
	}

	public static void main(String[] args) {
		String cmd = "";

		if (args.length < 6) {
			printHelp("Invalid number of arguments");
			return;
		} else if (args.length > 5
				&& (args[0].compareToIgnoreCase("-c") != 0 || args[2].compareToIgnoreCase("-i") != 0
						|| args[4].compareToIgnoreCase("-o") != 0 || (args.length > 6 && args[6].compareToIgnoreCase("-v") != 0))) {
			System.out.println(Integer.toString(args.length));
			System.out.println(args[0]);
			System.out.println(args[2]);
			System.out.println(args[4]);
			printHelp("Arguments order should be exactly like below");
			return;
		}
		if (args[1].compareToIgnoreCase("xml") == 0 || args[1].compareToIgnoreCase("sql") == 0
				|| args[1].compareToIgnoreCase("sqlite") == 0 || args[1].compareToIgnoreCase("mp3") == 0)
			cmd = args[1];
		else {
			printHelp("Unknown command '" + args[1] + "'");
			return;
		}
		if (!fileExists(args[3])) {
			printHelp("Input file '" + args[3] + "' does not exist");
			return;
		} else if (args[3].compareToIgnoreCase(args[5]) == 0) {
			printHelp("Input and outout file cannot be the same");
			return;
		}
		verbose = args.length == 7;

		Dictionary dictionary = traverse(new File(args[3]));

		if (cmd.compareToIgnoreCase("xml") == 0)
			saveXML(dictionary, args[5]);
		else if (cmd.compareToIgnoreCase("sql") == 0)
			saveSql(dictionary, args[5]);
		else if (cmd.compareToIgnoreCase("sqlite") == 0)
			saveSqlite(dictionary, args[5]);
		else if (cmd.compareToIgnoreCase("mp3") == 0)
			saveMp3(dictionary, args[5]);

	}

	public static void printHelp(String hint) {
		String appName = "java -jar lexicon2sqlite.jar";
		if (hint.length() > 0)
			System.out.print(hint + ". ");
		System.out.println("Usage: " + appName + " [OPTIONS]\n");
		System.out.println("Read an folket-lexicon XML file and save do disk as xml,sql script or Sqlite database\n");
		System.out.println("Mandatory arguments are:");
		System.out.println("	-c	<xml|sql|sqlite|mp3>	process the input file then create an output file as xml|sql|mp3 script/database");
		System.out.println("	-i	<file>			the input XML file name to be processed");
		System.out.println("	-o	<file>			the output file name; specify it according to choosen -c option\n");
		System.out.println("Optional arguments are:");
		System.out.println("	-v	verbose	output the progress to screen\n");
		System.out.println("Report bugs to <eugenmihailescux@gmail.com>");
	};

	public static void saveMp3(Dictionary dictionary, String outFile) {
		FileWriter filew = null;
		start = System.nanoTime();
		VERBOSE("Creating the Mp3 file extractor " + outFile + "...");
		try {
			filew = new FileWriter(outFile);
			filew.write(dictionary.toMp3Download());
			filew.close();
			DONE();
		} catch (IOException e) {
			ERROR(e);
		}

	}

	public static void saveSql(final Dictionary dictionary, final String outFile) {
		FileWriter filew = null;
		start = System.nanoTime();
		VERBOSE("Creating the SQL script file: " + outFile + "...");
		try {
			filew = new FileWriter(outFile);
			filew.write(dictionary.toSqlCreateTable(null) + dictionary.toSqlInsert());
			filew.close();
			DONE();
		} catch (IOException e) {
			ERROR(e);
		}
	};

	public static void saveSqlite(final Dictionary dictionary, final String dbName) {

		new Runnable() {

			@Override
			public void run() {
				start = System.nanoTime();
				String fFileName = dbName + ".sql";
				saveSql(dictionary, fFileName);
				(new File(dbName)).delete();
				SQLiteConnection db = new SQLiteConnection(new File(dbName));
				try {
					VERBOSE("Creating Sqlite database...");
					db.open(true);
					DONE();

					VERBOSE("Populating database with records from '" + fFileName + "'...\n");
					String stmt;
					StringBuilder text = new StringBuilder();
					Scanner scanner;
					try {
						scanner = new Scanner(new FileInputStream(fFileName));
						final int count = getFileCount(fFileName);
						int i = 0, perc = 0, oldperc = -1;
						try {
							while (scanner.hasNextLine()) {
								stmt = scanner.nextLine();
								if (stmt.length() > 0 && !stmt.startsWith("#") && !stmt.startsWith("\n"))
									text.append(stmt);
								if (text.length() > 0 && text.toString().endsWith(";")) {
									stmt = text.toString();
									++i;
									if (count > 0) {
										perc = 100 * i / count;

										if (perc != oldperc || i >= count) {
											VERBOSE(String.format("\rExecuting statement # %d of %d (~ %d %%) ...", i, count, perc));
											oldperc = perc;
										}
									}
									db.exec(stmt);
									text.setLength(0);
								}
							}

						} finally {
							scanner.close();
						}
					} catch (FileNotFoundException e) {
						ERROR(e);
					}
					DONE();

				} catch (SQLiteException e) {
					ERROR(e);
				}

				db.dispose();
			}
		}.run();
	}

	public static void saveXML(Dictionary dictionary, String outFile) {
		FileWriter filew = null;
		start = System.nanoTime();
		VERBOSE("Creating the XML file: " + outFile + "...");
		try {
			filew = new FileWriter(outFile);
			filew.write(dictionary.toString());
			filew.close();
			DONE();
		} catch (IOException e) {
			ERROR(e);
		}

	}

	private static Dictionary traverse(File xmlFile) {
		FolketLexiconSaxParser handler = null;
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			handler = new FolketLexiconSaxParser();
			parser.parse(xmlFile, handler);

		} catch (Exception e) {
			ERROR(e);
		}

		// if we don't initialize the fields than they will be initialized only
		// if the XML file provides coresponding elements; this is not good
		// because our sqlite table structure is created dinamically based on
		// the dictionary field's values
		if (handler.dictionary.comment == null)
			handler.dictionary.comment = "";
		if (handler.dictionary.created == null)
			handler.dictionary.created = "";
		if (handler.dictionary.last_changed == null)
			handler.dictionary.last_changed = "";
		if (handler.dictionary.license == null)
			handler.dictionary.license = "";
		if (handler.dictionary.licenseComment == null)
			handler.dictionary.licenseComment = "";
		if (handler.dictionary.name == null)
			handler.dictionary.name = "";
		if (handler.dictionary.originURL == null)
			handler.dictionary.originURL = "";
		if (handler.dictionary.source_language == null)
			handler.dictionary.source_language = "";
		if (handler.dictionary.target_language == null)
			handler.dictionary.target_language = "";
		if (handler.dictionary.version == null)
			handler.dictionary.version = "";

		return handler.dictionary;
	}

	public static void VERBOSE(String s) {
		if (verbose)
			System.out.print(s);
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		new String(ch, start, length);
	}

}
